# Import
import os

import matplotlib.axes
import sklearn.base
from pandas import DataFrame
# from google.colab import drive
from sklearn.tree import DecisionTreeClassifier

from sklearn.ensemble import BaggingClassifier, ExtraTreesClassifier, \
    RandomForestClassifier

from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from sklearn.neural_network import MLPClassifier

from sklearn.tree import plot_tree
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

import pandas as pd

import seaborn as sns
import math
import matplotlib.pyplot as plt

from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler

from tqdm import tqdm
from time import time

import sys, getopt

from concurrent.futures import ThreadPoolExecutor

# Constants

CSV_NAME = "2023-11-30-fixed"

FILE_TIME = time()
OUTPUT_DIR = f"output/{FILE_TIME}"
os.makedirs(OUTPUT_DIR)

MODEL = "DecisionTreeClassifier"
LOOPS = 10
TEST_SIZE = 0.2

APPLY_OVERSAMPLING = True
POST_OVERSAMPLING = False
RUN_ALL = True
RUN_RELATION = True
RUN_REMOVAL = True
RUN_NEW_REMOVAL = True
BIN_COUNT = 2

ESTIMATORS_COUNT = 100
CORE_COUNT = os.cpu_count() - 1
THREAD_POOL = None


def create_pool():
    return ThreadPoolExecutor(max_workers=CORE_COUNT)


# Read Arguments

opts, args = getopt.getopt(
    sys.argv[1:],
    "m:l:c:hd:b:",
    [
        "norelation",
        "noremoval",
        "noall",
        "estimators",
        "nonewremoval",
        "help",
        "no-oversampling"
    ]
)

for opt, arg in opts:
    if opt == "-m":
        MODEL = arg
    elif opt == "-l":
        LOOPS = int(arg)
    elif opt == "-c":
        CORE_COUNT = int(arg)
    elif opt == "-b":
        BIN_COUNT = int(arg)
    elif opt == "-d":
        CSV_NAME = arg
    elif opt == "--norelation":
        RUN_RELATION = False
        print("Not running relation test")
    elif opt == "--noremoval":
        RUN_REMOVAL = False
        print("Not running removal test")
    elif opt == "--noall":
        RUN_ALL = False
        print("Not running all test")
    elif opt == "--no-oversampling":
        APPLY_OVERSAMPLING = False
        print("Not applying oversampling.")
    elif opt == "--post-oversampling":
        POST_OVERSAMPLING = True
        print("Applying oversampling after train/test split.")
    elif opt == "--nonewremoval":
        RUN_NEW_REMOVAL = False
        print("Not running all test")
    elif opt == "--estimators":
        ESTIMATORS_COUNT = int(arg)
        print(f"Configured with {arg} estimators")
    elif opt in ["-h", "--help"]:
        print(
            "Machine Learning Process\n"
            "Basic usage: python test.py\n\n"
            "Options:\n"
            "\t-l=INT\t\t\tHow many loops to execute, will be squared. (default: 10)\n"
            "\t-m=STR\t\t\tWhich model to use. (default: DecisionTreeClassifier)\n"
            "\t-c=INT\t\t\tHow many threads to use. (default: nproc - 1 or 1 depending on model)\n"
            "\t-b=INT\t\t\tCount of bins. (default: 2)\n"
            "\t-d=STR\t\t\tFilename of dataset to use, should be in same directory as script."
            " (default: 2023-11-30-fixed)\n"
            "\t--estimators=INT\tHow many estimators to use for RandomForestClassifier. (broken)\n"
            "\t--norelation\t\tDo not run the relation test.\n"
            "\t--noremoval\t\tDo not run the removal test.\n"
            "\t--noall\t\t\tDo not run the all test.\n"
            "\t--nonewremoval\t\tDo not run the new feature removal test.\n"
            "\t--no-oversampling\t\tDo not apply oversampling.\n"
            "\t--post-oversampling\t\tApply oversampling after splitting data into train/test.\n"
            "\t-h, --help\t\tShow this message."
        )
        exit(0)

print(f"Testing model {MODEL} x {LOOPS ** 2}")

# Only 1 core due to multi threading support
if MODEL == "KNeighborsClassifier":
    CORE_COUNT = 1

# Read CSV

# music_data = pd.read_csv("/content/drive/MyDrive/SURI Summer23/SRC/Data/" + CSV_NAME + ".csv")
music_data: DataFrame = pd.read_csv("./" + CSV_NAME + ".csv")
music_data = music_data.loc[:, ~music_data.columns.str.contains('^Unnamed')]

# Analyse CSV

X = music_data.drop(columns="popularity")
y = music_data["popularity"]
data = pd.concat([X, y], axis=1)

y_name = 'popularity'
features_names = [f'{label}' for label in list(X.columns)]
column_names = features_names + [y_name]
data.columns = column_names

g = sns.FacetGrid(pd.DataFrame(features_names), col=0, col_wrap=6, sharex=False)
for ax, x_var in zip(g.axes, features_names):
    sns.scatterplot(data=data, x=x_var, y=y_name, ax=ax)
g.tight_layout()

plt.savefig(f"{OUTPUT_DIR}/scatter.png")

plt.figure(figsize=(20, 10))

ax = sns.heatmap(music_data.corr(), annot=True)

plt.savefig(f"{OUTPUT_DIR}/heatmap.png")


## Test the varience

def test_variance() -> pd.Series:
    music_data_copy = music_data.copy()
    pd.options.display.float_format = '{:2,.02f}'.format
    return music_data_copy.var(numeric_only=True)


test_variance().to_csv(f"{OUTPUT_DIR}/variance.txt")

# Manipulate

source_x = music_data.drop(columns="popularity")

source_y = music_data["popularity"]

## Binning

if BIN_COUNT == 2:
    BIN_LABELS = ["not popular", "very popular"]
elif BIN_COUNT == 3:
    BIN_LABELS = ["not popular", "moderately popular", "very popular"]
elif BIN_COUNT == 4:
    BIN_LABELS = ["not popular", "somewhat popular", "popular", "very popular"]
else:
    print("Invalid BIN Count")
    exit(1)

man_x = source_x
man_y = pd.cut(source_y, bins=BIN_COUNT, labels=BIN_LABELS, right=True)

## Oversampling

if APPLY_OVERSAMPLING and not POST_OVERSAMPLING:
    ros: RandomOverSampler = RandomOverSampler()
    man_x, man_y = ros.fit_resample(man_x, man_y)

## Finalize

music_data_x: DataFrame = man_x
music_data_y: DataFrame = man_y


# Training and Testing


def test(given_x: DataFrame) -> float:
    f"""
    Run the testing suite.
    Executes {LOOPS} ^ 2 amount of trainings, predictions, and verifications
    :param given_x: x data to use
    :return: tuple between the model used, and the average score of that model
    """
    total_score = 0
    start_time = time()

    with tqdm(total=LOOPS ** 2, desc="Performing tests") as progress:
        with create_pool() as pool:
            for n in range(LOOPS):
                # Split the data up
                x_train, x_test, y_train, y_test = train_test_split(given_x, music_data_y, test_size=TEST_SIZE)

                if APPLY_OVERSAMPLING and POST_OVERSAMPLING:
                    # noinspection PyShadowingNames
                    ros: RandomOverSampler = RandomOverSampler()
                    x_train, y_train = ros.fit_resample(x_train, y_train)
                for i in range(LOOPS):
                    def task():
                        # Tree
                        if MODEL == "DecisionTreeClassifier":
                            test_model = DecisionTreeClassifier()
                        elif MODEL == "BaggingClassifier":
                            test_model = BaggingClassifier()
                        elif MODEL == "ExtraTreesClassifier":
                            test_model = ExtraTreesClassifier()
                        elif MODEL == "RandomForestClassifier":
                            test_model = RandomForestClassifier(n_estimators=100)
                        elif MODEL == "KNeighborsClassifier":
                            test_model = KNeighborsClassifier(n_neighbors=1)

                        # Fit data into model
                        # print("Fitting")
                        test_model.fit(x_train, y_train)

                        # print("Predicting")
                        prediction = test_model.predict(x_test)
                        # print("Checking accuracy")
                        score = accuracy_score(y_test, prediction)
                        # print(f"Score: {score}")
                        nonlocal total_score
                        total_score += score
                        # print(f"Total: {total_score}")
                        progress.update(1)

                    pool.submit(task)
            # Await for all jobs to complete
            # print("Waiting for jobs")
    result = (total_score / (LOOPS * LOOPS))

    end_time = time()
    print(f"Total Time: {end_time - start_time}")

    return result


def save_progress(
        file_path: str,
        model: str,
        columns: str,
        average_score: float,
        arguments: str = "",
):
    with open(f"{OUTPUT_DIR}/{file_path}.txt", 'a') as file:
        file.write(
            f"{model},{arguments},{CSV_NAME},{music_data_y.shape[0]},{BIN_COUNT},{TEST_SIZE},{columns},{average_score}\n")


def test_all():
    """
    Test with all features
    """
    average_score = test(music_data_x)

    save_progress(
        file_path="test_all-results",
        model=MODEL,
        columns="x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x",
        average_score=average_score
    )


def test_feature_removal():
    """
    Test removing one feature at a time
    """
    for column in music_data_x.columns:
        music_data_x_v2 = music_data_x.drop(columns=[column])
        average_score = test(music_data_x_v2)
        column_row = ""
        for field in music_data_x.columns:
            if field != column:
                column_row += "x,"
            else:
                column_row += ","

        save_progress("test_feature_removal-results", MODEL, column_row, average_score)


def test_feature_relation():
    """
    Test the relation of one feature at a time
    The relation being the accuracy of the model with that feature
    """
    for column in music_data_x.columns:
        music_data_x_v2 = music_data_x[[column]]
        average_score = test(music_data_x_v2)
        column_row = ""
        for field in music_data_x.columns:
            if field == column:
                column_row += "x,"
            else:
                column_row += ","

        save_progress("test_feature_relation-results", MODEL, column_row, average_score)


def test_new_removal():
    """
    Test removal of the three new features
    """
    REMOVED = ["age", "artist_lifespan", "artist_relevance"]
    music_data_x_v2 = music_data_x.drop(columns=REMOVED)
    average_score = test(music_data_x_v2)
    column_row = ""
    for field in music_data_x.columns:
        if field not in REMOVED:
            column_row += "x,"
        else:
            column_row += ","

    save_progress("test_new_removal", MODEL, column_row, average_score)


if RUN_ALL:
    test_all()

if RUN_REMOVAL:
    test_feature_removal()

if RUN_RELATION:
    test_feature_relation()

if RUN_NEW_REMOVAL:
    test_new_removal()
